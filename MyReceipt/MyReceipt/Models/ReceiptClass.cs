﻿namespace MyReceipt.Models
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Receipt
    {
        [JsonProperty("invoice_id")]
        public long InvoiceId { get; set; }

        [JsonProperty("created_date")]
        public string CreatedDate { get; set; }

        [JsonProperty("created_hour")]
        public string CreatedHour { get; set; }

        [JsonProperty("merchand_name")]
        public string MerchandName { get; set; }

        [JsonProperty("merchand_city")]
        public string MerchandCity { get; set; }

        [JsonProperty("items")]
        public ObservableCollection<Item> Items { get; set; }

        [JsonProperty("items_total_price")]
        public double ItemsTotalPrice { get; set; }
    }

    public partial class Item
    {
        [JsonProperty("item_price")]
        public double ItemPrice { get; set; }

        [JsonProperty("item_title")]
        public string ItemTitle { get; set; }

        [JsonProperty("item_quantity")]
        public long ItemQuantity { get; set; }
    }

    public partial class Receipt
    {
        public static List<Receipt> FromJson(string json) => JsonConvert.DeserializeObject<List<Receipt>>(json, MyReceipt.Models.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this List<Receipt> self) => JsonConvert.SerializeObject(self, MyReceipt.Models.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
