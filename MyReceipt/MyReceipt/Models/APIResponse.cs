﻿using System;
namespace MyReceipt.Models
{
    public class ApiResponse
    {
        public string status { get; set; }
        public string msg { get; set; }
        public string err_msg { get; set; }
        public string token { get; set; }
    }
}
