﻿using MyReceipt.Views;
using MyReceipt.Models;
using MyReceipt.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Services;

namespace MyReceipt.ViewModels
{
    public class ReceiptPageViewModel : ViewModelBase
    {
        public DelegateCommand ReceiptListRefreshCommand { get; set; }
        public DelegateCommand<Receipt> ReceiptTappedCommand { get; set; }

        ReceiptsAPIService _receiptService;

        private ObservableCollection<Receipt> _receipts;
        public ObservableCollection<Receipt> Receipts
        {
            get { return _receipts; }
            set { SetProperty(ref _receipts, value); }
        }

        private bool _isRefreshing;
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set { SetProperty(ref _isRefreshing, value); }
        }

        public ReceiptPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            ReceiptListRefreshCommand = new DelegateCommand(OnPullToRefresh);
            ReceiptTappedCommand = new DelegateCommand<Receipt>(OnReceiptTapped);

            Title = "My Receipts";
            IsRefreshing = true;


        }

        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);

            if (parameters.ContainsKey("tokenObjectKey"))
            {
                var token = parameters["tokenObjectKey"] as string;

                _receiptService = new ReceiptsAPIService(_navigationService, _pageDialogService, token);
                RefreshReceiptListAsync();
            }
            else
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatingTo)}:  No receiptObjectKey found.");
            }
        }

        private async void OnPullToRefresh()
        {
            await RefreshReceiptListAsync();
        }

        private async void OnReceiptTapped(Receipt receiptTapped)
        {
            NavigationParameters navParams = new NavigationParameters();
            navParams.Add("receiptObjectKey", receiptTapped);
            await _navigationService.NavigateAsync(nameof(DetailPage), navParams, false, true);
        }

        private async Task RefreshReceiptListAsync()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(RefreshReceiptListAsync)}");
            try
            {
                Receipts = await _receiptService.GetReceiptAsync("/user/home");
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(RefreshReceiptListAsync)}: EXCEPTION!!! {ex}");

            }
            finally
            {
                IsRefreshing = false;
            }
        }
    }
}