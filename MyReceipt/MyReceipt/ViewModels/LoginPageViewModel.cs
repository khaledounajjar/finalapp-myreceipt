﻿using MyReceipt.Models;
using MyReceipt.Services;
using MyReceipt.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyReceipt.ViewModels
{
    public class LoginPageViewModel : ViewModelBase
    {
        private string _email;
        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }
        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }
          

        public DelegateCommand NavigateToRegisterPageCommand { get; set; }
        public DelegateCommand GoLoginCommand { get; set; }

        ReceiptsAPIService _receiptService;


        public LoginPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            Title = "Login";
            NavigateToRegisterPageCommand = new DelegateCommand(OnNavigateToRegisterPage);
            GoLoginCommand = new DelegateCommand(OnGoLogin);
            _receiptService = new ReceiptsAPIService(navigationService, pageDialogService, "");
        }

        private void OnNavigateToRegisterPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigateToRegisterPage)}");
            _navigationService.NavigateAsync(nameof(RegisterPage));
        }

        private async void OnGoLogin()
        {
            await PostCheck();
        }

        private async Task PostCheck()
        {
            ApiResponse apiResponse = null;
            Debug.WriteLine($"**** Clicked on Login: Email= '{_email}' && Password = '{_password}' ");
            if (string.IsNullOrEmpty(_email) || string.IsNullOrEmpty(_password))
            {
                await _pageDialogService.DisplayAlertAsync("Error",
                                                           "Please enter your email and your password.",
                                                           "Cancel",
                                                           "OK");
                return;
            }
            try
            {
                string myJsonBody = string.Format("{{\"email\":\"{0}\",\"password\":\"{1}\"}}", _email, _password);
                apiResponse = await _receiptService.PostReceiptAsync("/account/login", myJsonBody);
                 if (apiResponse != null && apiResponse.status == "success" && apiResponse.msg == "Success login")
                 {
                    NavigationParameters navParams = new NavigationParameters();
                    navParams.Add("tokenObjectKey", apiResponse.token);
                    await _navigationService.NavigateAsync(nameof(ReceiptPage), navParams, false, true);
                 }
                 else
                 {
                    if (apiResponse != null)
                    {
                        await _pageDialogService.DisplayAlertAsync("Error",
                                                                   $"{apiResponse.err_msg}",
                                                                    "Cancel",
                                                                    "OK");
                    }
                 }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostCheck)}: EXCEPTION!!! {ex}");
            }
        }

    }
}
