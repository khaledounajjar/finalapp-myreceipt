﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using MyReceipt.Models;
using Prism.Navigation;
using Prism.Services;

namespace MyReceipt.ViewModels
{
    public class DetailPageViewModel : ViewModelBase
    {
        private ObservableCollection<Item> _itemList;
        public ObservableCollection<Item> ItemList
        {
            get { return _itemList; }
            set { SetProperty(ref _itemList, value); }
        }

        public DetailPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
        }

        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);
            Receipt receipt = null;

            if (parameters.ContainsKey("receiptObjectKey"))
            {
                receipt = parameters["receiptObjectKey"] as Receipt;
                ItemList = receipt.Items;
            }
            else
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatingTo)}:  No receiptObjectKey found.");
            }
        }
    }
}
