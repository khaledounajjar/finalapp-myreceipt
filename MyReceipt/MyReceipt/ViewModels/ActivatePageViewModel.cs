﻿using MyReceipt.Views;
using MyReceipt.Models;
using MyReceipt.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Services;

namespace MyReceipt.ViewModels
{
    public class ActivatePageViewModel : ViewModelBase
    {
        
        private string _code;
        public string Code
        {
            get { return _code; }
            set { SetProperty(ref _code, value); }
        }

        public DelegateCommand NavigateToLoginPageCommand { get; set; }
        public DelegateCommand GoActivateCommand { get; set; }

        ReceiptsAPIService _receiptService;

        public ActivatePageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            Title = "Activate";

            NavigateToLoginPageCommand = new DelegateCommand(OnNavigateToLoginPage);
            GoActivateCommand = new DelegateCommand(OnActivateAccount);
        }

        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigateToLoginPage)}");
            var navParams = new NavigationParameters();
            base.OnNavigatingTo(parameters);

            if (parameters.ContainsKey("tokenObjectKey"))
            {
                var token = parameters["tokenObjectKey"] as string;
                Debug.WriteLine($"OnNavigatingToActivate*** : TOKEN= '{token}' ");

                _receiptService = new ReceiptsAPIService(_navigationService, _pageDialogService, token);
            }
            else
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatingTo)}:  No receiptObjectKey found.");
            }
        }

        private void OnNavigateToLoginPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigateToLoginPage)}");
            _navigationService.GoBackToRootAsync();
        }
        private async void OnActivateAccount()
        {
            await PostCheck();
        }

        private async Task PostCheck()
        {
            ApiResponse apiResponse = null;
            Debug.WriteLine($"ActivatePage*** Clicked on Activate: Code= '{_code}' ");
            if (string.IsNullOrEmpty(_code))
            {
                await _pageDialogService.DisplayAlertAsync("Error",
                                                           "Please enter the code you received on your email.",
                                                           "Cancel",
                                                           "OK");
                return;
            }
            try
            {
                apiResponse = await _receiptService.GetActivateAsync($"/account/activate/{_code}");
                if (apiResponse != null && apiResponse.status == "success" && apiResponse.msg == "Account activated")
                {
                    await _pageDialogService.DisplayAlertAsync("You can Login",
                                                                   $"{apiResponse.msg}",
                                                                    "Cancel",
                                                                    "OK");
                    _navigationService.GoBackToRootAsync();
                }
                else
                {
                    if (apiResponse != null)
                    {
                        await _pageDialogService.DisplayAlertAsync("Error",
                                                                   $"{apiResponse.err_msg}",
                                                                    "Cancel",
                                                                    "OK");
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostCheck)}: EXCEPTION!!! {ex}");
            }
        }
    }
}