﻿using MyReceipt.Views;
using MyReceipt.Models;
using MyReceipt.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Services;

namespace MyReceipt.ViewModels
{
    public class RegisterPageViewModel : ViewModelBase
    {
        private string _email;
        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }
        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        public DelegateCommand NavigateToLoginPageCommand { get; set; }
        public DelegateCommand GoSignUpCommand { get; set; }

        ReceiptsAPIService _receiptService;
       
        public RegisterPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            Title = "Register";

            NavigateToLoginPageCommand = new DelegateCommand(OnNavigateToLoginPage);
            GoSignUpCommand = new DelegateCommand(OnSignUp);
            _receiptService = new ReceiptsAPIService(navigationService, pageDialogService, "");
     

        }

        private void OnNavigateToLoginPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigateToLoginPage)}");
            _navigationService.GoBackAsync();
        }
        private async void OnSignUp()
        {
            await PostCheck();
        }

        private async Task PostCheck()
        {
            ApiResponse apiResponse = null;
            Debug.WriteLine($"RegisterPage*** Clicked on SignUp: Email= '{_email}' && Password = '{_password}' ");
            if (string.IsNullOrEmpty(_email) || string.IsNullOrEmpty(_password))
            {
                await _pageDialogService.DisplayAlertAsync("Error",
                                                           "Please enter your email and your password.",
                                                           "Cancel",
                                                           "OK");
                return;
            }
            try
            {
                string myJsonBody = string.Format("{{\"email\":\"{0}\",\"password\":\"{1}\"}}", _email, _password);
                apiResponse = await _receiptService.PostReceiptAsync("/account/register", myJsonBody);
                if (apiResponse != null && apiResponse.status == "success" && apiResponse.msg == "Account created")
                {
                    NavigationParameters navParams = new NavigationParameters();
                    navParams.Add("tokenObjectKey", apiResponse.token);
                    await _navigationService.NavigateAsync(nameof(ActivatePage), navParams, false, true);
                }
                else
                {
                    if (apiResponse != null)
                    {
                        await _pageDialogService.DisplayAlertAsync("Error",
                                                                   $"{apiResponse.err_msg}",
                                                                    "Cancel",
                                                                    "OK");
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostCheck)}: EXCEPTION!!! {ex}");
            }
        }
    }
}