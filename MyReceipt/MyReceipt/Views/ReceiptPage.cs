﻿using System;

using Xamarin.Forms;

namespace MyReceipt.Views
{
    public class ReceiptPage : ContentPage
    {
        public ReceiptPage()
        {
            Content = new StackLayout
            {
                Children = {
                    new Label { Text = "Hello ContentPage" }
                }
            };
        }
    }
}

