﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using MyReceipt.Models;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using System.Text;
using MyReceipt.ViewModels;
using Prism.Navigation;
using Prism.Services;

namespace MyReceipt.Services
{
    public class ReceiptsAPIService : ViewModelBase
    {
        private string _baseUrl = "http://s2.youss.eu:3000";

        private HttpClient _httpClient;
        private string _token;


        public ReceiptsAPIService(INavigationService navigationService, IPageDialogService pageDialogService, string token)
            : base(navigationService, pageDialogService)
        {
            _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _httpClient.Timeout = TimeSpan.FromSeconds(5);
            _token = token;

        }

        public bool DoIHaveInternet()
        {
            if (!CrossConnectivity.IsSupported)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(DoIHaveInternet)}:  Not supported... Returning true no matter what.");
                return true;
            }

            bool hasNet = CrossConnectivity.Current.IsConnected;
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(DoIHaveInternet)}:  returning {hasNet}");
            return hasNet;
        }

        public async Task<ObservableCollection<Receipt>> GetReceiptAsync(string requestUrl)
        {
            if (DoIHaveInternet() == false)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetReceiptAsync)}:  Returning null... We do not have internet.");
                await _pageDialogService.DisplayAlertAsync("Error",
                                                                           "Please check your internet connection, then restart the app.",
                                                                           "Cancel",
                                                                           "OK");
                return null;
            }

            ObservableCollection<Receipt> receiptObs = null;
            var request = new HttpRequestMessage();

            request.Method = HttpMethod.Get;
            request.RequestUri = new Uri($"{_baseUrl}{requestUrl}");
            request.Headers.Add("auth-token", _token);

            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetReceiptAsync)}:  Sending request: {request.RequestUri.ToString()}");

            try
            {

                using (HttpResponseMessage httpResponseMessage = await _httpClient.SendAsync(request))
                {
                    if (httpResponseMessage.IsSuccessStatusCode == false)
                    {
                        Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetReceiptAsync)}:  Request Failed. Status code: {httpResponseMessage.IsSuccessStatusCode.ToString()}");
                    }

                    string responseAsString = await httpResponseMessage.Content.ReadAsStringAsync();
                    Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetReceiptAsync)}:  JSON Response: {responseAsString}");

                    receiptObs = JsonConvert.DeserializeObject<ObservableCollection<Receipt>>(responseAsString);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetReceiptAsync)}:  EXCEPTION!!\n: {ex}");
                await _pageDialogService.DisplayAlertAsync("Error",
                                                                           "A server error occured, try to restart the app.",
                                                                           "Cancel",
                                                                           "OK");
            }
            return receiptObs;
        }

        public async Task<ApiResponse> PostReceiptAsync(string requestUrl, string myJsonBody)
        {
            if (DoIHaveInternet() == false)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostReceiptAsync)}:  Returning null... We do not have internet.");
                await _pageDialogService.DisplayAlertAsync("Error",
                                                                           "Please check your internet connection, then restart the app.",
                                                                           "Cancel",
                                                                           "OK");
                return null;
            }
            var request = new HttpRequestMessage();
            ApiResponse resultDeserialized = new ApiResponse();

            request.Method = HttpMethod.Post;
            request.RequestUri = new Uri($"{_baseUrl}{requestUrl}");
            request.Content = new StringContent(myJsonBody, Encoding.UTF8, "application/json");

            try
            {
                using (HttpResponseMessage httpResponseMessage = await _httpClient.SendAsync(request))
                {
                    if (httpResponseMessage.IsSuccessStatusCode == false)
                    {
                        Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostReceiptAsync)}:  Request Failed. Status code: {httpResponseMessage.IsSuccessStatusCode.ToString()}");
                    }
                    string responseAsString = await httpResponseMessage.Content.ReadAsStringAsync();
                    Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostReceiptAsync)}:  JSON Response: {responseAsString}");

                    resultDeserialized = JsonConvert.DeserializeObject<ApiResponse>(responseAsString);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostReceiptAsync)}:  EXCEPTION!!\n: {ex}");
                await _pageDialogService.DisplayAlertAsync("Error",
                                                           "A server error occured, try to restart the app.",
                                                           "Cancel",
                                                           "OK");
            }
            return resultDeserialized;
        }

        public async Task<ApiResponse> GetActivateAsync(string requestUrl)
        {
            if (DoIHaveInternet() == false)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetActivateAsync)}:  Returning null... We do not have internet.");
                await _pageDialogService.DisplayAlertAsync("Error",
                                                                           "Please check your internet connection, then restart the app.",
                                                                           "Cancel",
                                                                           "OK");
                return null;
            }

            ApiResponse resultDeserialized = new ApiResponse();
            var request = new HttpRequestMessage();

            request.Method = HttpMethod.Get;
            request.RequestUri = new Uri($"{_baseUrl}{requestUrl}");
            request.Headers.Add("auth-token", _token);
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetActivateAsync)}: TOOOOKEN: {_token}");

            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetActivateAsync)}:  Sending request: {request.RequestUri.ToString()}");

            try
            {

                using (HttpResponseMessage httpResponseMessage = await _httpClient.SendAsync(request))
                {
                    if (httpResponseMessage.IsSuccessStatusCode == false)
                    {
                        Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetActivateAsync)}:  Request Failed. Status code: {httpResponseMessage.IsSuccessStatusCode.ToString()}");
                    }

                    string responseAsString = await httpResponseMessage.Content.ReadAsStringAsync();
                    Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetActivateAsync)}:  JSON Response: {responseAsString}");

                    resultDeserialized = JsonConvert.DeserializeObject<ApiResponse>(responseAsString);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetActivateAsync)}:  EXCEPTION!!\n: {ex}");

            }
            return resultDeserialized;
        }

    }
}

